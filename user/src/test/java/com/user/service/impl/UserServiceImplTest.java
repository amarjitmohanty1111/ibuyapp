package com.user.service.impl;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Optional;

import com.user.serviceimpl.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.user.entity.User;
import com.user.exception.InvalidUserCredentials;
import com.user.repository.UserRepository;

@ExtendWith(SpringExtension.class)
public class UserServiceImplTest {

	@InjectMocks
	UserServiceImpl userServiceImpl;

	@Mock
	UserRepository userRepository;

	@Mock
	private BCryptPasswordEncoder bcryptEncoder;

	@Test
	public void TestloadUserByUsernameSuccess() {
		String username ="veeru";
		User user = new User();
		user.setEmailId(username);
		user.setPassword("veer08908");

		Mockito.when(userRepository.findByEmailId(username)).thenReturn(user);
		UserDetails response = userServiceImpl.loadUserByUsername(username);

		assertEquals("veeru", response.getUsername());

	}

	@Test
	public void TestloadUserByUsernameFailure() {
		String username ="ravi";
		User user = new User();
		Mockito.when(userRepository.findByEmailId(username)).thenReturn(null);

		UsernameNotFoundException thrown = Assertions.assertThrows(UsernameNotFoundException.class, () -> {
			userServiceImpl.loadUserByUsername(username);
		});				
		Assertions.assertEquals("Invalid username or password",thrown.getMessage());

	}

	@Test
	public void TestFindOneSuccess() {

		String userId ="vb@gmail.com";
		User user = new User();
		user.setEmailId("vb@gmail.com");
		user.setPassword("veer08908");

		Mockito.when(userRepository.findByEmailId(userId)).thenReturn(user);

		User response = userServiceImpl.findOne(userId);
		assertEquals("vb@gmail.com", response.getEmailId());
	}

	@Test
	public void TestFindOneFailure() {
		String userId = "veeru";

		Mockito.when(userRepository.findByEmailId(userId)).thenReturn(null);

		User response = userServiceImpl.findOne(userId);
		assertNull(response);
	}

	@Test
	public void TestValidateUserByRequestSuccess() {
		String email="vb@gmail.com";
		Long requestUserId=(long) 1;
		
		User user = new User();
		user.setEmailId("vb@gmail.com");
		Mockito.when(userRepository.findById(requestUserId)).thenReturn(Optional.of(user));
		
		boolean response = userServiceImpl.validateUserByRequest(email, requestUserId);
		assertTrue(response);
		
	}
	
	@Test
	public void TestValidateUserByRequestFailure() {
		String email="vb@gmail.com";
		Long requestUserId=(long) 1;
		
		Optional<User> user = Optional.empty();
		Mockito.when(userRepository.findById(requestUserId)).thenReturn(user);
				
		InvalidUserCredentials thrown = Assertions.assertThrows(InvalidUserCredentials.class, () -> {
			userServiceImpl.validateUserByRequest(email, requestUserId);
		});				
		Assertions.assertEquals("Not authorized to perform operation",thrown.getMessage());
		
	}
}
