package com.user.controller;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.user.config.JwtTokenUtil;
import com.user.service.UserService;

@ExtendWith(SpringExtension.class)
public class AuthenticationControllerTest {

	@InjectMocks
	AuthenticationController authenticationController;
	
	@Mock
	UserService userService;
	
	AuthenticationManager authenticationManager;

	@Mock
	JwtTokenUtil jwtTokenUtil;
	
	//@Test
	public void testAuthentication() {
		
	}
}
