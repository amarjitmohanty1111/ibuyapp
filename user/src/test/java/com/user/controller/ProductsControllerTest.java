package com.user.controller;

import com.user.dto.Response;
import com.user.dto.StatusEnum;
import com.user.exception.ProductsNotFoundException;
import com.user.serviceimpl.ProductServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class ProductsControllerTest {

    @InjectMocks
    ProductController controller;

    @Mock
    ProductServiceImpl service;

    Response response = new Response();

    @Test
    void searchProductsHappyPathTest() throws ProductsNotFoundException {
        response.setApiStatus(StatusEnum.SUCCESS);
        Mockito.when(service.searchProducts(Mockito.anyString())).thenReturn(response);
        Assertions.assertEquals(HttpStatus.OK, controller.searchProducts("hp").getStatusCode());
    }

    @Test
    void searchProductsWhenNotFoundTest() throws ProductsNotFoundException {
        response.setApiStatus(StatusEnum.FAIL);
        Mockito.when(service.searchProducts(Mockito.anyString())).thenReturn(response);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, controller.searchProducts("hp").getStatusCode());
    }
}