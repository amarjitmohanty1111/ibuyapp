package com.user.serviceimpl;

import com.user.dto.Response;
import com.user.dto.StatusEnum;
import com.user.exception.ProductsNotFoundException;
import com.user.feignClient.ProductApi;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class ProductServiceImplTest {

    @InjectMocks
    ProductServiceImpl service;

    @Mock
    ProductApi productApi;

    Response response = new Response();

    @Test
    void searchProductsHappyPathTest() throws ProductsNotFoundException {

        response.setApiStatus(StatusEnum.SUCCESS);
        Mockito.when(productApi.serchProducts(Mockito.anyString())).thenReturn(new ResponseEntity<>(response, HttpStatus.OK));
        Assertions.assertEquals(StatusEnum.SUCCESS, service.searchProducts("hp").getApiStatus());
    }

}