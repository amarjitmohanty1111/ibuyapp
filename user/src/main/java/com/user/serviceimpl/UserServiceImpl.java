package com.user.serviceimpl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.user.entity.User;
import com.user.exception.InvalidUserCredentials;
import com.user.repository.UserRepository;
import com.user.service.UserService;




@Service("userService")
public class UserServiceImpl implements UserService,UserDetailsService{

	@Autowired
	UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	private final Logger logger = LogManager.getLogger(UserServiceImpl.class);


	public UserDetails loadUserByUsername(String username){
		User userInfo = userRepository.findByEmailId(username);
		if(userInfo!=null){
			return new org.springframework.security.core.userdetails.User(userInfo.getEmailId(), userInfo.getPassword(), getAuthority());
		}else {
			logger.error("Invalid username or password");
			throw new UsernameNotFoundException("Invalid username or password");
		}
	}

	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
	}

	@Override
	public User findOne(String email) {
		User user = userRepository.findByEmailId(email);
		if(user!=null) {
			return user;
		}else {
			return null;
		}
	}

	@Override
	public boolean validateUserByRequest(String email, Long requestUserId) throws InvalidUserCredentials {
		Optional<User> userDetails = userRepository.findById(requestUserId);
		if(userDetails.isPresent()) {
			return userDetails.get().getEmailId().equals(email);
		}else {
			throw new InvalidUserCredentials("Not authorized to perform operation");
		}
	}

}