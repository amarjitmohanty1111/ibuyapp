package com.user.serviceimpl;

import com.user.dto.Response;
import com.user.exception.ProductsNotFoundException;
import com.user.feignClient.ProductApi;
import com.user.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductApi productsApi;

    @Override
    public Response searchProducts(String search) throws ProductsNotFoundException {
        ResponseEntity<Response> productsResponse;
        try{
            productsResponse = productsApi.serchProducts(search);
        } catch (Exception exception) {
            throw new ProductsNotFoundException("Products Not Found For Specified Search");
        }
        Response response = productsResponse.getBody();
        return response;
    }
}
