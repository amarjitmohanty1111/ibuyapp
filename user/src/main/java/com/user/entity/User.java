package com.user.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class User implements Serializable {

	@Id
	@GeneratedValue
	private Long userId;

	private String emailId;

	private String password;

	private String firstName;

	private String lastName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	private String mobile;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private List<Product> listOfProducts;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private List<Cart> listOfCarts;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private List<Purchase> listOfPurchases;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public List<Product> getListOfProducts() {
		return listOfProducts;
	}

	public void setListOfProducts(List<Product> listOfProducts) {
		this.listOfProducts = listOfProducts;
	}


	public List<Cart> getListOfCarts() {
		return listOfCarts;
	}

	public void setListOfCarts(List<Cart> listOfCarts) {
		this.listOfCarts = listOfCarts;
	}


	public List<Purchase> getListOfPurchases() {
		return listOfPurchases;
	}

	public void setListOfPurchases(List<Purchase> listOfPurchases) {
		this.listOfPurchases = listOfPurchases;
	}

}
