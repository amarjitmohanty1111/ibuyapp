package com.user.exception;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

import com.user.dto.ErrorMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.user.dto.ErrorResponse;




@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	private final Logger logger = LogManager.getLogger(GlobalExceptionHandler.class);
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String message = error.getDefaultMessage();
            errors.put(fieldName, message);
        });
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(InvalidUserCredentials.class)
	public ResponseEntity<ErrorResponse> handleExceptio(InvalidUserCredentials ex){
		ErrorResponse validationErrorResponse = new ErrorResponse();
		validationErrorResponse.setStatusCode(HttpStatus.UNAUTHORIZED.value());
		validationErrorResponse.setMessage(ex.getMessage());
		return new ResponseEntity<>(validationErrorResponse,HttpStatus.UNAUTHORIZED);
	}
	
	@ExceptionHandler(MyApplicationException.class)
	public ResponseEntity<Object> handleMyBankException(MyApplicationException ex) {
		logger.error("Exception occured with"+ex.getLocalizedMessage());
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatusCode(500);
		return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> handleExceptio(SQLIntegrityConstraintViolationException ex){
		ErrorResponse validationErrorResponse = new ErrorResponse();
		validationErrorResponse.setStatusCode(500);
		validationErrorResponse.setMessage("unable to register user with given details");
		return new ResponseEntity<>(validationErrorResponse,HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(ProductsNotFoundException.class)
	public ResponseEntity<ErrorMessage> productsNotFoundException(ProductsNotFoundException exception, WebRequest request) {
		ErrorMessage errorMessage = new ErrorMessage(HttpStatus.NOT_FOUND, exception.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMessage);
	}
 
}
