package com.user.controller;


import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import static com.user.constants.Constants.TOKEN_PREFIX;


import com.user.config.JwtTokenUtil;
import com.user.dto.AuthToken;
import com.user.dto.AuthenticationRequest;
import com.user.entity.User;
import com.user.exception.InvalidUserCredentials;
import com.user.service.UserService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;


@RestController
public class AuthenticationController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtTokenUtil jwtTokenUtil;

	@Autowired
	UserService userService;

	private final Logger logger = LogManager.getLogger(AuthenticationController.class);

	
	@Operation(summary = "user authentication",description = "As a iBuy application user i can perform authentication",tags="Post")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "jwt token",
					content= {@Content(mediaType = "application/json",schema =@Schema(implementation =User.class ))}),
			@ApiResponse(responseCode = "401" , description = "Invalid Credentials",content = @Content)
	})
	@PostMapping("/login")
	public ResponseEntity<AuthToken> authentication(@RequestBody @Valid AuthenticationRequest loginUser) {	
		try {
			logger.info("Checking the authentication...");
			final Authentication authentication = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(
							loginUser.getEmailId(),
							loginUser.getPassword()
							)
					);
			final User user = userService.findOne(loginUser.getEmailId());
			SecurityContextHolder.getContext().setAuthentication(authentication);
			logger.info("Generating the JWT Token");
			final String token = jwtTokenUtil.generateToken(user);
			return new ResponseEntity<>(new AuthToken(token, user.getEmailId()), HttpStatus.OK);
		}catch (AuthenticationException e) {
			logger.error("Invaild Credentials");
			throw new InvalidUserCredentials("Invaild Credentials");

		}

	}
	
	@SecurityRequirement(name = "iBuyapp")
	@GetMapping("/hello/{userId}")
	public ResponseEntity<String> hello(@PathVariable("userId") Long userId, @RequestHeader (name="Authorization") String token) {
		String emailId = jwtTokenUtil.getUsernameFromToken(token.replace(TOKEN_PREFIX,""));

		if(userService.validateUserByRequest(emailId, userId)) {
					return new ResponseEntity<>("Success",HttpStatus.OK);

		}else {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

		}
	}

}