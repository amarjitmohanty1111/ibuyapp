package com.user.controller;

import com.user.dto.Response;
import com.user.dto.StatusEnum;
import com.user.exception.ProductsNotFoundException;
import com.user.serviceimpl.ProductServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Objects;

@RestController
@RequestMapping("/products")
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    ProductServiceImpl service;

    @GetMapping("/")
    public ResponseEntity<Response> searchProducts(@RequestParam @NotNull @Pattern(regexp = "^[a-zA-Z0-9]*$", message = "Special Characters Not Allowed") String search) throws ProductsNotFoundException {
        HttpStatus httpStatus = null;
        Response response = service.searchProducts(search);
        if (!Objects.isNull(response) && !response.getApiStatus().equals(StatusEnum.SUCCESS)) {
            httpStatus = HttpStatus.NOT_FOUND;
        } else {
            LOGGER.info("FETCHED PRODUCTS SUCCESSFULLY");
            httpStatus = HttpStatus.OK;
        }
        return new ResponseEntity<>(response, httpStatus);
    }
}
