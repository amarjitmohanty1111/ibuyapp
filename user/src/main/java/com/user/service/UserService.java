package com.user.service;

import com.user.entity.User;
import com.user.exception.InvalidUserCredentials;



public interface UserService {

	User findOne(String username);

	boolean validateUserByRequest(String email, Long requestUserId) throws InvalidUserCredentials;

}