package com.user.service;

import com.user.dto.Response;
import com.user.exception.ProductsNotFoundException;

public interface ProductService {

    Response searchProducts(String search) throws ProductsNotFoundException;
}
