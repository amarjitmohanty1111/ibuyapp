package com.user.purchase.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.user.entity.Purchase;

@Repository
public interface PurchaseRepository {

	@Query("SELECT * FROM (SELECT * FROM ibuy.purchase ORDER BY purchase_id DESC LIMIT ?1)Var1 ORDER BY purchase_id ASC;")
	List<Purchase> getPurchases(int numberOfOrders);

}
