package com.user.feignClient;


import com.user.dto.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@FeignClient(value = "stockApiResponse", url = "http://localhost:8080/ibuyappproduct")
public interface ProductApi {

    @GetMapping("/products/")
    public ResponseEntity<Response> serchProducts(@RequestParam @NotNull @Pattern(regexp = "^[a-zA-Z0-9]*$", message = "Special Characters Not Allowed") String search);
}
