package com.user.dto;

public class AuthToken {

    private String token;
    private String userId;

    public AuthToken(){

    }

    public AuthToken(String token, String userId){
        this.token = token;
        this.userId = userId;
    }

    public AuthToken(String token){
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

 
}
