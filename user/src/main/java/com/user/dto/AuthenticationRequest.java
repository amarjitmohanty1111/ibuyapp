package com.user.dto;

import javax.validation.constraints.NotEmpty;


public class AuthenticationRequest {

	@NotEmpty
	private String emailId;
	
	@NotEmpty
	private String password;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
}