package com.user.userenum;

public enum CartStatus {

	PURCHASED {
		@Override
		public String toString() {
			return "purchased";
		}
	},
	CANCELLED {
		@Override
		public String toString() {
			return "cancelled";
		}
	}

}

