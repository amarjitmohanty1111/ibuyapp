package com.product.controller;

import com.product.dto.Response;
import com.product.dto.StatusEnum;
import com.product.exception.InvalidSearchException;
import com.product.serviceimpl.ProductsServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class ProductControllerTest {

    @InjectMocks
    ProductController controller;

    @Mock
    ProductsServiceImpl service;

    Response response = new Response();

    @Test
    void searchProductsHappyPathTest() throws InvalidSearchException {
        response.setApiStatus(StatusEnum.SUCCESS);
        Mockito.when(service.searchProducts(Mockito.anyString())).thenReturn(response);
        Assertions.assertEquals(HttpStatus.OK, controller.searchProducts("hp").getStatusCode());
    }

    @Test
    void searchProductWhenNotFoundTest() throws InvalidSearchException {
        response.setApiStatus(StatusEnum.FAIL);
        Mockito.when(service.searchProducts(Mockito.anyString())).thenReturn(response);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, controller.searchProducts("hp").getStatusCode());
    }

}