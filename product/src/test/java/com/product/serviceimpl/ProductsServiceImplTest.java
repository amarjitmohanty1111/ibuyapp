package com.product.serviceimpl;

import com.product.dto.StatusEnum;
import com.product.entity.Product;
import com.product.exception.InvalidSearchException;
import com.product.repository.ProductsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
class ProductsServiceImplTest {

    @InjectMocks
    ProductsServiceImpl service;

    @Mock
    ProductsRepository repository;

    @Mock
    ModelMapper modelMapper;

    @Test
    void searchProductsHappyPathTest() throws InvalidSearchException {
        List<Product> productsList = new ArrayList<Product>();
        Product product = new Product();
        product.setProduct_id(12l);
        product.setDescription("pc");
        product.setName("hp");
        productsList.add(product);

        Mockito.when(repository.findByNameContainingOrMakeContainingOrDescriptionContaining(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(productsList);
        Assertions.assertEquals(StatusEnum.SUCCESS, service.searchProducts("hp").getApiStatus());
    }

    @Test
    void searchProductsWhenExceptionThrownTest() throws InvalidSearchException {

        Mockito.when(repository.findByNameContainingOrMakeContainingOrDescriptionContaining(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(new ArrayList<>());
        Assertions.assertThrows(InvalidSearchException.class, ()-> {
            service.searchProducts("hp");
        });
    }
}