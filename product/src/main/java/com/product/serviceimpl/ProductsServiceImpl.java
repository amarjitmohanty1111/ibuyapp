package com.product.serviceimpl;

import com.product.dto.ProductsResponseDto;
import com.product.dto.Response;
import com.product.dto.StatusEnum;
import com.product.entity.Product;
import com.product.exception.InvalidSearchException;
import com.product.repository.ProductsRepository;
import com.product.service.ProductsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductsServiceImpl implements ProductsService {

    @Autowired
    ProductsRepository repository;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public Response searchProducts(String search) throws InvalidSearchException {
        Response response = new Response();
        List<Product> productsList = repository.findByNameContainingOrMakeContainingOrDescriptionContaining(search,search,search);
        List<ProductsResponseDto> productsDtoList  = productsList.stream().map(product -> modelMapper.map(product, ProductsResponseDto.class)).collect(Collectors.toList());
        if (productsDtoList.isEmpty()) {
            throw new InvalidSearchException("Invalid Search! Please Enter Correct Search Details");
        }
        response.setApiStatus(StatusEnum.SUCCESS);
        response.setResponseBody(productsDtoList);
        return response;
    }
}
