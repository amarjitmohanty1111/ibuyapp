package com.product.dto;


public class Response {

    StatusEnum apiStatus;
    Object responseBody;

    public Response() {
    }

    public Response(StatusEnum apiStatus, Object responseBody) {
        this.apiStatus = apiStatus;
        this.responseBody = responseBody;
    }

    public StatusEnum getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(StatusEnum apiStatus) {
        this.apiStatus = apiStatus;
    }

    public Object getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(Object responseBody) {
        this.responseBody = responseBody;
    }
}
