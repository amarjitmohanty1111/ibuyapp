package com.product.dto;

public class ProductsResponseDto {

    String name;
    String make;
    String description;
    double price;
    Long discount;
    int availableQuantity;

    public ProductsResponseDto() {
    }

    public ProductsResponseDto(String name, String make, String description, double price, Long discount, int availableQuantity) {
        this.name = name;
        this.make = make;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.availableQuantity = availableQuantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getDiscount() {
        return discount;
    }

    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }
}
