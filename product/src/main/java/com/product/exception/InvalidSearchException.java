package com.product.exception;

public class InvalidSearchException extends Exception {

    public InvalidSearchException() {
        super();
    }

    public InvalidSearchException(String message) {
        super(message);
    }

    public InvalidSearchException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidSearchException(Throwable cause) {
        super(cause);
    }

    protected InvalidSearchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
