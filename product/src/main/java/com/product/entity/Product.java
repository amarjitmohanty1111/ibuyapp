package com.product.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Product {

    @Id
    Long product_id;
    String name;
    String make;
    String description;
    double price;
    Long discount;
    int availableQuantity;


    public Product() {
    }

    public Product(Long product_id, String name, String make, String description, double price, Long discount, int availableQuantity) {
        this.product_id = product_id;
        this.name = name;
        this.make = make;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.availableQuantity = availableQuantity;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getDiscount() {
        return discount;
    }

    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }
}
