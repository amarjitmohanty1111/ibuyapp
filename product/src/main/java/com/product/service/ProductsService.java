package com.product.service;

import com.product.dto.Response;
import com.product.exception.InvalidSearchException;

public interface ProductsService {
    Response searchProducts(String search) throws InvalidSearchException;
}
